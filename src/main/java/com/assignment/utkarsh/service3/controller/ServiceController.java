package com.assignment.utkarsh.service3.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.utkarsh.service3.model.Name;
import com.assignment.utkarsh.service3.utils.interfaces.IPrintJson;

@RestController
public class ServiceController {

	private static final Logger logger = LoggerFactory.getLogger(ServiceController.class);

	@Autowired
	IPrintJson printJson;

	@PostMapping("/name")
	public ResponseEntity<String> getName(@RequestBody Name name) {
		printJson.printJson(logger, "Received to Service 3 with payload", name);
		return new ResponseEntity<String>(name.getName() + " " + name.getSurname(), HttpStatus.OK);
	}

}
