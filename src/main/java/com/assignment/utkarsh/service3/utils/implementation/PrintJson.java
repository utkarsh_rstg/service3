package com.assignment.utkarsh.service3.utils.implementation;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.assignment.utkarsh.service3.utils.interfaces.IPrintJson;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PrintJson implements IPrintJson {

	@Autowired
	ObjectMapper objectMapper;

	@Override
	public void printJson(Logger logger, String name, Object object) {
		try {
			if (logger.isTraceEnabled()) {
				logger.trace("{} : {}", name, objectMapper.writeValueAsString(object));
			}

		} catch (JsonProcessingException ex) {
			logger.error("Error while printing {} : {} ", name, ex);
		}
	}

}
