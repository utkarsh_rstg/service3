package com.assignment.utkarsh.service3.model;

import org.springframework.lang.NonNull;

public class Name {
	
	public Name(String firstName, String surname) {
		this.firstName = firstName;
		this.surname = surname;
	}

	@NonNull
	private String firstName;

	@NonNull
	private String surname;

	public String getName() {
		return firstName;
	}

	public void setName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	@Override
	public String toString() {
		return this.firstName + " "+this.surname;
	}

}
