package com.assignment.utkarsh.service3.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.assignment.utkarsh.service3.utils.implementation.PrintJson;
import com.assignment.utkarsh.service3.utils.interfaces.IPrintJson;

@Configuration
public class Config {

	@Bean
	public IPrintJson getPrintJson() {

		return new PrintJson();
	}

}
