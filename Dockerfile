FROM openjdk:8
EXPOSE 8082
ADD target/centime-service3.jar centime-service3.jar
ENTRYPOINT ["java","-jar","/centime-service3.jar"]