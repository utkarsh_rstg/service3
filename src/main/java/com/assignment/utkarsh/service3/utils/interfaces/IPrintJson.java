package com.assignment.utkarsh.service3.utils.interfaces;

import org.slf4j.Logger;

public interface IPrintJson {

	public void printJson(Logger logger, String name, Object object);
}
